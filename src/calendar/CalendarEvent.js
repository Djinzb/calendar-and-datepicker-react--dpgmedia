import React from 'react';
import { CalendarEventStyled } from './styles';

const CalendarEvent = props => {
  return <CalendarEventStyled>{props.title}</CalendarEventStyled>;
};

export default CalendarEvent;
