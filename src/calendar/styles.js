import styled from 'styled-components';

export const CalendarStyled = styled.div`
  background-color: white;
  text-align: center;
  .active {
    border: 2px solid #ffc20f;
  }
`;

export const CalendarHeader = styled.div`
  display: flex;
  height: 4rem;
  align-items: center;
  padding: 0.5rem 1rem;
  border-bottom: 1px solid #dbdbdb;
`;

export const CalendarHeaderButtons = styled.span`
  button {
    border: none;
    font-size: 1.3rem;
  }
`;

export const CalendarHeaderDate = styled.span`
  margin-left: 2rem;
  font-family: Helvetica;
  font-size: 1.875rem;
  font-weight: bold;
  &::first-letter {
    text-transform: uppercase;
  }
`;

export const CalendarGrid = styled.div`
  .calendar__grid {
    &-weekdays {
      display: grid;
      grid-template-columns: repeat(7, 1fr);
      background-color: #dbdbdb;
      grid-column-gap: 1px;
      height: 1.5rem;
    }
    &-weekday {
      background: white;
      font-family: Helvetica;
      font-size: 11px;
      padding-top: 0.7rem;
    }
    &-days {
      background-color: #dbdbdb;
      display: grid;
      grid-gap: 1px;
      grid-template-columns: repeat(auto-fill, minmax(calc(100vw / 8), 1fr));
    }

    &-day {
      background: white;
      padding-top: 1rem;
      font-family: Helvetica;
      font-size: 14px;
      font-weight: bold;
      min-height: calc((100vh / 6) - 1px);
      overflow-y: auto;
    }
    &-day:nth-child(-n + 7) {
      padding-top: 0.2rem;
    }
  }
  .notInMonth {
    color: rgba(0, 0, 0, 0.3);
  }
`;

export const CalendarEventStyled = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 90%;
  background-color: #e5f4fb;
  margin: 0.3125rem 0.625rem;
  padding: 0.625rem;
  border-radius: 0.1875rem;
`;
