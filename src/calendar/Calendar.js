import React, { useState } from 'react';
import dayjs from 'dayjs';
import 'dayjs/locale/nl';
// import CalendarEvent from './CalendarEvent';
import {
  CalendarStyled,
  CalendarHeader,
  CalendarHeaderButtons,
  CalendarHeaderDate,
  CalendarGrid,
} from './styles';

const Calendar = props => {
  const [dateObject, setDateObject] = useState(dayjs().locale('nl'));

  const getDaysInMonth = () => {
    return dateObject.daysInMonth();
  };
  const getCurrentDay = () => {
    return Number(dateObject.format('D'));
  };
  const getCurrentMonth = () => {
    return dateObject.format('MMMM');
  };
  const getCurrentYear = () => {
    return dateObject.format('YYYY');
  };
  const getFirstDayOfMonth = () => {
    let firstDay = dayjs(dateObject)
      .startOf('month')
      .format('d');
    return firstDay - 1;
  };
  const prevMonth = () => {
    setDateObject(dayjs(dateObject.subtract(1, 'month')));
  };
  const nextMonth = () => {
    setDateObject(dayjs(dateObject.add(1, 'month')));
  };
  const getWeekdaysHeader = () => {
    const weekdayshort = ['MA', 'DI', 'WO', 'DO', 'VR', 'ZA', 'ZO'];
    const weekdaysHeader = [];
    weekdayshort.forEach((day, i) => {
      weekdaysHeader.push(
        <span className="calendar__grid-weekday" key={'weekdays' + i}>
          {day}
        </span>
      );
    });
    return weekdaysHeader;
  };

  const renderDaysOfLastMonth = () => {
    let blanks = [];
    const amountOfDaysPrevMonth = dateObject.subtract(1, 'month').daysInMonth();
    for (let i = 1; i <= getFirstDayOfMonth(); i++) {
      let date =
        amountOfDaysPrevMonth -
        getFirstDayOfMonth() +
        i +
        dateObject.subtract(1, 'month').format('MM') +
        getCurrentYear();

      blanks.push(
        <div key={`notInMonth` + i} className="calendar__grid-day notInMonth">
          {amountOfDaysPrevMonth - getFirstDayOfMonth() + i}
          {/* {filterPlansByDate(date)} */}
        </div>
      );
    }
    return blanks;
  };

  const renderDaysInMonth = () => {
    const days = [];
    let daysOfLastMonth = renderDaysOfLastMonth();
    let daysOfNextMonth = [];
    let date = 0;

    for (let d = 1; d <= getDaysInMonth(); d++) {
      if (d < 10) {
        date = '0' + d + dateObject.format('MM') + getCurrentYear();
      } else {
        date = d + dateObject.format('MM') + getCurrentYear();
      }

      let currentDay;
      if (
        d === getCurrentDay() &&
        dayjs()
          .locale('nl')
          .format('MMMM') === getCurrentMonth()
      ) {
        currentDay = 'active';
      } else {
        currentDay = '';
      }
      days.push(
        <div key={d} className={`calendar__grid-day ${currentDay}`} id={date}>
          <span>{d}</span>
          {/* {filterPlansByDate(date)} */}
        </div>
      );
    }

    let amountOfDays = daysOfLastMonth.length + days.length;
    for (let i = 1; i <= 35 - amountOfDays; i++) {
      date =
        '0' + i + dateObject.add(1, 'month').format('MM') + getCurrentYear();
      daysOfNextMonth.push(
        <div
          key={`notInMonthEnd` + i}
          className="calendar__grid-day notInMonth"
        >
          {i}
          {/* {filterPlansByDate(date)} */}
        </div>
      );
    }
    let totalDays = [...daysOfLastMonth, ...days, ...daysOfNextMonth];

    return totalDays;
  };

  // const filterPlansByDate = date => {
  //   const plans = props.plans;
  //   return plans.map((plan, i) => {
  //     if (plan.date === date) {
  //       return <CalendarEvent title={plan.title} key={`plan` + i} />;
  //     } else {
  //       return null;
  //     }
  //   });
  // };
  return (
    <CalendarStyled>
      <CalendarHeader>
        <CalendarHeaderButtons>
          {getCurrentMonth() ===
          dayjs()
            .locale('nl')
            .format('MMMM') ? (
            <React.Fragment>
              <button onClick={() => prevMonth()} disabled>
                &#60;
              </button>
              <button onClick={() => nextMonth()}>&#62;</button>
            </React.Fragment>
          ) : getCurrentMonth() ===
            dayjs()
              .add(1, 'month')
              .locale('nl')
              .format('MMMM') ? (
            <React.Fragment>
              <button onClick={() => prevMonth()}>&#60;</button>
              <button onClick={() => nextMonth()} disabled>
                &#62;
              </button>
            </React.Fragment>
          ) : null}
        </CalendarHeaderButtons>
        <CalendarHeaderDate>
          {getCurrentMonth() + ' ' + getCurrentYear()}
        </CalendarHeaderDate>
      </CalendarHeader>
      <CalendarGrid>
        <div className="calendar__grid-weekdays">{getWeekdaysHeader()}</div>
        <div className="calendar__grid-days">{renderDaysInMonth()}</div>
      </CalendarGrid>
    </CalendarStyled>
  );
};

export default Calendar;
