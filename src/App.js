import React from 'react';
import { BrowserRouter, Route, Link } from 'react-router-dom';
import DatePicker from './datepicker/DatePicker';
import Calendar from './calendar/Calendar';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Link to="/calendar">Calendar</Link>
        <Link to="/datepicker">Datepicker</Link>
      </div>

      <Route path="/calendar" component={Calendar} />
      <Route path="/datepicker" component={DatePicker} />
    </BrowserRouter>
  );
}

export default App;
