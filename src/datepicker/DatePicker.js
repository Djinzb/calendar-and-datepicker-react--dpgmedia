import React, { useState } from 'react';
import dayjs from 'dayjs';
import './DatePicker.scss';
import 'dayjs/locale/nl';

const DatePicker = () => {
  const [calendarOpen, setCalendarOpen] = useState(false);
  const [dateObject, setDateObject] = useState(dayjs().locale('nl'));
  const [selectedDate, setSelectedDate] = useState(
    dateObject.format('DD/MM/YYYY')
  );

  const getDaysInMonth = () => {
    return dateObject.daysInMonth();
  };
  const getCurrentMonth = () => {
    return dateObject.format('MMMM');
  };
  const getCurrentYear = () => {
    return dateObject.format('YYYY');
  };
  const getFirstDayOfMonth = () => {
    let firstDay = dayjs(dateObject)
      .startOf('month')
      .format('d');
    return firstDay - 1;
  };

  const prevMonth = () => {
    setDateObject(dayjs(dateObject.subtract(1, 'month')));
  };
  const nextMonth = () => {
    setDateObject(dayjs(dateObject.add(1, 'month')));
  };

  const getWeekdaysHeader = () => {
    const weekdayshort = ['MA', 'DI', 'WO', 'DO', 'VR', 'ZA', 'ZO'];
    const weekdaysHeader = [];
    weekdayshort.forEach((day, i) => {
      weekdaysHeader.push(
        <span className="datepicker__grid__weekday" key={'weekdays' + i}>
          {day}
        </span>
      );
    });
    return weekdaysHeader;
  };

  const renderDaysOfLastMonth = () => {
    let blanks = [];
    const amountOfDaysPrevMonth = dateObject.subtract(1, 'month').daysInMonth();
    for (let i = 1; i <= getFirstDayOfMonth(); i++) {
      let date =
        amountOfDaysPrevMonth -
        getFirstDayOfMonth() +
        i +
        '/' +
        dateObject.subtract(1, 'month').format('MM') +
        '/' +
        getCurrentYear();

      blanks.push(
        <div
          key={`notInMonth` + i}
          className={`datepicker__grid__day notInMonth ${
            pickSelectedDate(date) ? 'active' : null
          }`}
          onClick={e => {
            setSelectedDate(e.target.getAttribute('data-date'));
            setCalendarOpen(false);
          }}
          data-date={date}
        >
          {amountOfDaysPrevMonth - getFirstDayOfMonth() + i}
        </div>
      );
    }
    return blanks;
  };

  const renderDaysInMonth = () => {
    const days = [];
    let daysOfLastMonth = renderDaysOfLastMonth();
    let daysOfNextMonth = [];
    let date = 0;

    for (let d = 1; d <= getDaysInMonth(); d++) {
      if (d < 10) {
        date = '0' + d + '/' + dateObject.format('MM') + '/' + getCurrentYear();
      } else {
        date = d + '/' + dateObject.format('MM') + '/' + getCurrentYear();
      }

      days.push(
        <div
          key={d}
          className={`datepicker__grid__day ${
            pickSelectedDate(date) ? 'active' : null
          }`}
          data-date={date}
          onClick={e => {
            setSelectedDate(e.target.getAttribute('data-date'));
            setCalendarOpen(false);
          }}
        >
          {d}
        </div>
      );
    }

    let amountOfDays = daysOfLastMonth.length + days.length;
    for (let i = 1; i <= 35 - amountOfDays; i++) {
      date =
        '0' +
        i +
        '/' +
        dateObject.add(1, 'month').format('MM') +
        '/' +
        getCurrentYear();
      daysOfNextMonth.push(
        <div
          key={`notInMonthEnd` + i}
          data-date={date}
          className={`datepicker__grid__day notInMonth ${
            pickSelectedDate(date) ? 'active' : null
          }`}
          onClick={e => {
            setSelectedDate(e.target.getAttribute('data-date'));
            setCalendarOpen(false);
          }}
        >
          {i}
        </div>
      );
    }
    let totalDays = [...daysOfLastMonth, ...days, ...daysOfNextMonth];

    return totalDays;
  };

  const pickSelectedDate = date => {
    if (selectedDate === date) {
      return true;
    } else {
      return false;
    }
  };
  return (
    <React.Fragment key="datepicker">
      <div className="datepicker">
        <div className="datepicker__input">
          <span className="datepicker__input--date">
            <label>Datum</label>
            <input type="text" value={selectedDate} readOnly />
            {!calendarOpen ? (
              <span key="calendarOpen" onClick={() => setCalendarOpen(true)}>
                <svg
                  className="datepicker__input__icon"
                  id="calendar-item"
                  xmlns="http://www.w3.org/2000/svg"
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                >
                  <g
                    fill="none"
                    fillRule="evenodd"
                    stroke="#000"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    transform="translate(5 3)"
                  >
                    <rect width="14" height="15" y="2" rx="1" />
                    <path d="M4 0v4M10 0v4M1 7h13" />
                  </g>
                </svg>
              </span>
            ) : (
              <span key="calendarClose" onClick={() => setCalendarOpen(false)}>
                <svg
                  className="datepicker__input__icon"
                  onClick={() => setCalendarOpen(false)}
                  xmlns="http://www.w3.org/2000/svg"
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                >
                  <g fill="#000" fillRule="nonzero">
                    <path d="M7 19a1 1 0 0 1-.71-1.71l10-10a1.004 1.004 0 0 1 1.42 1.42l-10 10A1 1 0 0 1 7 19z" />
                    <path d="M17 19a1 1 0 0 1-.71-.29l-10-10a1.004 1.004 0 0 1 1.42-1.42l10 10A1 1 0 0 1 17 19z" />
                  </g>
                </svg>
              </span>
            )}
          </span>
          <span className="datepicker__input--time">
            <label>Tijd</label>
            <input type="time" defaultValue={'00:00'} />
          </span>
        </div>
        <div className="datepicker__flyout" id={calendarOpen ? 'show' : 'hide'}>
          <div className="datepicker__header">
            <span className="datepicker__header__month">
              {getCurrentMonth() + ' ' + getCurrentYear()}
            </span>
            <span className="datepicker__header__buttons">
              <button key="prevMonth" onClick={() => prevMonth()}>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                >
                  <path
                    fill="#000"
                    fillRule="nonzero"
                    d="M8 12a1 1 0 0 1 .29-.71l5-5a1.004 1.004 0 0 1 1.42 1.42L10.41 12l4.3 4.29a1.004 1.004 0 0 1-1.42 1.42l-5-5A1 1 0 0 1 8 12z"
                  />
                </svg>
              </button>
              <button key="nextMonth" onClick={() => nextMonth()}>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                >
                  <path
                    fill="#000"
                    fillRule="nonzero"
                    d="M16 12a1 1 0 0 1-.29.71l-5 5a1.004 1.004 0 0 1-1.42-1.42l4.3-4.29-4.3-4.29a1.004 1.004 0 0 1 1.42-1.42l5 5A1 1 0 0 1 16 12z"
                  />
                </svg>
              </button>
            </span>
          </div>
          <div className="datepicker__grid">
            <div className="datepicker__grid__weekdays">
              {getWeekdaysHeader()}
            </div>
            <div className="datepicker__grid__days">{renderDaysInMonth()}</div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default DatePicker;
